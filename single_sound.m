
% https://www.mathworks.com/matlabcentral/answers/281261-frequency-domain-analysis-of-a-audio-signal
% https://www.cs.tut.fi/sgn/arg/CQT/
% https://www.mathworks.com/matlabcentral/answers/284324-how-to-plot-absolute-frequency-response-of-filter


cd 'C:\Users\Lisa\Dropbox\7.Semester\Vertiefungsseminar\Pitch Detection\presentation'
[w, f] = audioread('A4_clarinet.wav'); % f = sample rate = 44,1 kHz
p = audioplayer(w, f);

df = f / length(w);

frequency_audio = -f/2:df:f/2-df;
 
W = fftshift(fft(w))/length(fft(w));
%W = fft(w);
plot(frequency_audio,abs(W));
title('FFT of Input Audio');
xlabel('Frequency(Hz)');
ylabel('Amplitude');

% Nyquist frequency
%freq_nyquist = 1/f;





%freqz(w(:,1));