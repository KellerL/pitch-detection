
% https://www.mathworks.com/matlabcentral/answers/336881-filtering-out-a-specific-sound-from-a-audio-file
% https://www.mathworks.com/matlabcentral/answers/14133-using-xcorr-in-pitch-detections

cd 'C:\Users\Lisa\Dropbox\7.Semester\Vertiefungsseminar\Pitch Detection\presentation'
[w, f] = audioread('Pitch Baseball Sound Effect.wav'); % f = sample rate = 44,1 kHz
p = audioplayer(w, f);

% x = linspace(0,f,length(w));
% 
% figure
% subplot(3,1,1) 
% plot(x, w(:,1));
% title('Time Domain of Audio File');
% xlabel('Sample Number');
% ylabel('Amplitude');

freqz(w(:,1));
%freqz(w(:,2));

% Fourier Transform
% W = fft(w);
% 
% % stem is plot for discret signals
% % abs() => magnitudes; angle() => phases
% 
% n = linspace(0,f/2,length(w));
% 
% subplot(3,1,2) 
% plot(n, abs(W));
% title('Frequenzy Domain of Audio File');
% xlabel('Sample Number');
% ylabel('Magnitude');
% %axis([0 60000 0 600])
% zoom xon;
% %zoom yon;
% 
% subplot(3,1,3) 
% plot(n, angle(W));
% xlabel('Sample Number');
% ylabel('Phase');




