function [L,M,pdlow,pdhigh,period,amdfvals,ssv]=...
    amdf_pitch(xin,fs,ss,Lmsec,Mmsec,pdlow,pdhigh,fid,amdfthresh)
%
% single frame analysis using amdf starting at sample ss in file
%
% Inputs:
%   xin: current speech file
%   fs: speech sampling rate
%   ss: starting sample for single frame analysis
%   Lmsec: frame length in msec; needs to be converted to samples
%   Mmsec: frame shift in msec; needs to be converted to samples
%   pdlow: lowest allowable pitch period in msec; needs to be converted to
%       samples
%   pdhigh: highest allowable pitch period in msec; needs to be converted
%       to samples
%   fid: file id of output text file
%   amdfthresh: threshold on amdf values for valid voiced frame
%
% Outputs:
%   L: frame length in samples
%   M: frame shift in samples
%   pdlow: lowest allowable pitch period in samples
%   pdhigh: highest allowable pitch period in samples
%   period: array with pitch periods on a frame-by-frame basis
%   amdfvals: values of amdf at period for each frame
%   ssv: starting samples for each analysis frame

% save entire speech file in x
    x=xin(1:length(xin));
    
% convert frame duration (from msec to samples) and frame shift (from msec
% to samples) by scaling by fs/1000
    L=floor(Lmsec*fs/1000);
    M=floor(Mmsec*fs/1000);
    
% convert maximum lag (from msec to samples) for autocorrelation computation 
% (corresponding to minimum pitch frequency) 
    pdhigh=floor(pdhigh*fs/1000);

% convert minimum lag (from msec to samples) for autocorrelation computation 
% (corresponding to maximum pitch frequency)
    pdlow=floor(pdlow*fs/1000);
    
% set threshold for amdf to be valid voiced frame
% amdfthresh=0.6;
    
% write out basic analysis parameters
    fprintf(fid,'window length: %d, window shift: %d\n',L,M);
    fprintf(fid,'shortest period: %d, longest period: %d \n',pdlow,pdhigh);
    
% measure log energy contour and input log energy threshold from peak
    eln=[];
    sse=1;
    ethresh=30;
    while (sse+L-1 < length(x))
        xs=x(sse:sse+L-1);
        eln=[eln 10*log10(sum(xs(1:L).^2))];
        sse=sse+M;
    end
    elnm=max(eln);
    elt=elnm-ethresh;
    
% accumulate pitch period values, along with minimum of amdf, in arrays
    period=[];
    amdfvals=[];
    ssv=[];
    ssr=1;
    while (ssr > 0)
        xs=x(ssr:ssr+L+pdhigh-1);
        
% check if log energy of frame > threshold; if not set frame to non-voiced
% with period 0, peak 0
        frame=floor(ssr/M)+1;
        if (eln(frame) > elt)            
            for k=1:pdhigh
                amdf(k)=sum(abs(xs(k:L+k-1)-xs(1:L)));
            end
            amdfmax=max(amdf);
            amdf=amdf/amdfmax;
        
% find amdf minimum above pdlow
            amdfmin=min(amdf(pdlow+1:pdhigh));
            xxx=find(amdf(pdlow:pdhigh) == amdfmin);
            minvalue=xxx(1)+pdlow-1;
            
% check for pitch period doubling
            if (floor((minvalue-1)/2) > pdlow && amdf(floor((minvalue-1)/2))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/2);
                amdfmin=amdf(minvalue-1);
            elseif (floor(minvalue/2) > pdlow && amdf(floor(minvalue/2))...
                    <= amdfmin+0.1)
                minvalue=1+floor(minvalue/2);
                amdfmin=amdf(minvalue-1);
            end
            
% check for pitch period tripling
            if (floor((minvalue-1)/3) > pdlow && amdf(floor((minvalue-1)/3))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/3);
                amdfmin=amdf(minvalue-1);
            elseif (floor((minvalue-1)/3+1) > pdlow && amdf(floor((minvalue-1)/3+1))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/3+1);
                amdfmin=amdf(minvalue-1);
            elseif (floor((minvalue-1)/3-1) > pdlow && amdf(floor((minvalue-1)/3-1))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/3-1);
                amdfmin=amdf(minvalue-1);
            end

% set period to 0 if amdfmin > amdfthresh=0.5
            if (amdfmin > amdfthresh)
                period=[period 0];
                amdfvals=[amdfvals amdfmin];
            else
                period=[period minvalue-1];
                amdfvals=[amdfvals amdfmin];
            end
            ssv=[ssv ssr];
            
        else
            
% save results in arrays
            minvalue=1;
            amdfmin=1;
            period=[period 0];
            amdfvals=[amdfvals 0];
            ssv=[ssv ssr];
            amdf(1:pdhigh)=0;
        end

        
% print out values in output file and on screen
        fprintf(fid,'ssr: %d, minvalue: %d, amdfmin: %6.2f \n',ssr,minvalue-1,amdfmin);
        ssr=ssr+M;
        if (ssr+L+pdhigh > length(x))
            ssr=0;
        end
    end
end