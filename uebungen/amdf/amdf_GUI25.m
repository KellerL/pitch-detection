function EditrunGui
% Modifiable runGUI file
clc;
clear all;

% USER  - ENTER FILENAME
fileName = 'amdf.mat';    %USER - ENTER FILENAME
fileData=load(fileName);
temp=fileData(1).temp;


f = figure('Visible','on',...
    'Units', 'normalized',...
    'Position',[0, 0, 1, 1],...
    'MenuBar', 'none',...
    'NumberTitle','off');

% %SENSE COMPUTER AND SET FILE DELIMITER
switch(computer)				
    case 'MACI64',		char= '/';
    case 'GLNX86',  char='/';
    case 'PCWIN',	char= '\';
    case 'PCWIN64', char='\';
    case 'GLNXA64', char='/';
end

% 
% find speech files directory by going up one level and down one level
% on the directory chain; as follows:
    dir_cur=pwd; % this is the current Matlab exercise directory path 
    s=regexp(dir_cur,char); % find the last '\' for the current directory
    s1=s(length(s)); % find last '\' character; this marks upper level directory
    dir_fin=strcat(dir_cur(1:s1),'speech_files'); % create new directory
    start_path=dir_fin; % save new directory for speech files location

% USER - ENTER PROPER CALLBACK FILE
Callbacks_amdf_GUI25(f,temp,start_path);
% Note comment PanelandBUttonCallbacks(f,temp) if panelAndButtonEdit is to
% be uncommented and used
end

% GUI for amdf pitch detector
% 2 Panels 
%   #1 - parameters
%   #2 - graphics
% 2 Graphics Panels
%   #1 - log energy plot
%   #2 - pitch period plot
% 1 TitleBox
% 9 Buttons
%   #1 - pushbutton - Speech Directory
%   #2 - popupmenu - Speech Files
%   #3 - editable button - ss: starting sample in file for frame analysis
%   #4 - editable button - Lmsec: frame duration in msec
%   #5 - editable button - Mmsec: frame shift in msec
%   #6 - popupmenu - male/female pitch period range
%   #7 - pushbutton - Run Single Frame Analysis
%   #8 - pushbutton - Run Pitch Detector
%   #9 - pushbutton - Close GUI