function [L,R,pdlow,pdhigh,xs,amdf,amdfmin,minvalue]=...
    amdf_frame(xin,fs,ss,Lm,Rm,pdlowm,pdhighm,fid)
%
% single frame analysis using amdf starting at sample ss in file
%
% Inputs:
%   xin: current speech file
%   fs: speech sampling rate
%   ss: starting sample for single frame analysis
%   Lm: frame length in msec; needs to be converted to samples
%   Rm: frame shift in msec; needs to be converted to samples
%   pdlowm: lowest allowable pitch period in msec; needs to be converted to
%       samples
%   pdhighm: highest allowable pitch period in msec; needs to be converted
%       to samples
%   fid: file id of output text file
%
% Outputs:
%   L: frame length in samples
%   R: frame shift in samples
%   pdlow: lowest allowable pitch period in samples
%   pdhigh: highest allowable pitch period in samples
%   xs: frame of signal for amdf analysis; xs(1:L+pdhigh)
%   amdf: amdf of frame of speech, normalized to peak of 1; amdf(1:pdhigh)
%   amdfmin: minimum value of amdf function between pdlow and pdhigh
%   minvalue: location of minimum value of amdf function

% save entire speech file in x
    x=xin(1:length(xin));
    
% convert frame duration (from msec to samples) and frame shift (from msec
% to samples) by scaling by fs/1000
    L=floor(Lm*fs/1000);
    R=floor(Rm*fs/1000);
    
% convert maximum lag (from msec to samples) for autocorrelation computation 
% (corresponding to minimum pitch frequency) 
    pdhigh=floor(pdhighm*fs/1000);

% convert minimum lag (from msec to samples) for autocorrelation computation 
% (corresponding to maximum pitch frequency)
    pdlow=floor(pdlowm*fs/1000);
    
% write out basic analysis parameters
    fprintf(fid,'window length: %d, window shift: %d\n',L,R);
    fprintf(fid,'shortest period: %d, longest period: %d \n',pdlow,pdhigh);
    
% compute modified amdf function  
	xs=x(ss:ss+L+pdhigh-1);

    for k=1:pdhigh
        amdf(k)=sum(abs(xs(k:L+k-1)-xs(1:L)));
    end
    amdfmax=max(amdf);
    amdf=amdf/amdfmax;
    
% find amdf minimum above pdlow
    amdfmin=min(amdf(pdlow+1:pdhigh));
    xxx=find(amdf(pdlow:pdhigh) == amdfmin);
    minvalue=xxx(1)+pdlow-1;
    
% check for pitch period doubling

            if (floor((minvalue-1)/2) > pdlow && amdf(floor((minvalue-1)/2))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/2);
                amdfmin=amdf(minvalue-1);
            elseif (floor(minvalue/2) > pdlow && amdf(floor(minvalue/2))...
                    <= amdfmin+0.1)
                minvalue=1+floor(minvalue/2);
                amdfmin=amdf(minvalue-1);
            end
            
% check for pitch period tripling
            if (floor((minvalue-1)/3) > pdlow && amdf(floor((minvalue-1)/3))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/3);
                amdfmin=amdf(minvalue-1);
            elseif (floor((minvalue-1)/3+1) > pdlow && amdf(floor((minvalue-1)/3+1))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/3+1);
                amdfmin=amdf(minvalue-1);
            elseif (floor((minvalue-1)/3-1) > pdlow && amdf(floor((minvalue-1)/3-1))...
                    <= amdfmin+0.1)
                minvalue=1+floor((minvalue-1)/3-1);
                amdfmin=amdf(minvalue-1);
            end

            period=minvalue-1;
end