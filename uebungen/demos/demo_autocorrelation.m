%% autocorrelation example
% signal
fs = 500;
T = 1/fs;
N = 250; % desired length of signal
t = (0:N-1)*T; %time vector 
f1 = 8; f2=f1*2; 
x = sin(2*pi*f1*t-pi/2) + sin(2*pi*f2*t);
% autocorrelation
illustrate_xcorr(x,x, 45)

%% 




% x_ = (sin(2*pi*f1*t-pi/2) + sin(2*pi*f2*t));
% x_ = 0.001* (x_ + 1.5 * randn(size(x_)));
% y = (sin(2*pi*f1*t-pi/2) + sin(2*pi*f2*t));
% y = 1000 * (y + 1.5 * randn(size(y)));
% figure
% subplot(2,1,1);
% plot(x_);
% [period_in_samples, frequency_Hz, cormx, lags] = calcFrequencyAutoCorr(x_, fs);
% subplot(2,1,2);
% plot(lags, cormx);
% title(['Autocorrolated Signal. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
% xlabel('Lag');
% ylabel('Correlation Measure');
% zoom xon
% 
% figure
% subplot(2,1,1);
% plot(y)
% [period_in_samples_y, frequency_Hz_y, cormy, lags_y] = calcFrequencyAutoCorr(x, fs);
% subplot(2,1,2);
% plot(lags_y, cormy);
% title(['Autocorrolated Signal. Period estimated = ' num2str(period_in_samples_y) ' samples (' num2str(frequency_Hz_y) 'Hz)']);
% xlabel('Lag');
% ylabel('Correlation Measure');
% zoom xon