% signal
fs = 44100;
T = 1/fs;
N = 3000; 
t = (0:N-1)*T; 
x = sin(2*pi*440*t); %A4
%x = sin(2*pi*29.135*t); % A#0
%x = sin(2*pi*103.83*t); % G#2
%x = sin(2*pi*261.63*t); % C4
%x = sin(2*pi*4186*t); % C8, octave error (autocorr)

figure
subplot(2,1,1);
plot(x);
title('Note');
xlabel('Sample');
ylabel('Amplitude');
zoom xon;





% plot autocorrelation
subplot(2,1,2);
xlabel('Lag');
ylabel('Autocorrelation Measure');
zoom xon;


