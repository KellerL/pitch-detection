%% signals
%cd '.\Audio'
[w, fs] = audioread('clarinet_A4_fortissimo.mp3');
%[w, fs] = audioread('contrabassoon_Gis3.mp3');
%[w, fs] = audioread('french-horn_A2.mp3');
%[w, fs] = audioread('cello_D5.mp3');
%[w, fs] = audioread('my_clarinet_imperial_march.wav');
%[w, fs] = audioread('my_clarinet_007.wav');


w = w(:,1); % only get first channel
p = audioplayer(w, fs);
p.play();
frame_length = round(fs*0.03); % length of the analysis frame, 30 ms