function [period_in_samples, frequency_Hz, d] = calcFrequencyAMDF(cur_frame, fs)

% https://dsp.stackexchange.com/questions/29962/how-to-deal-with-low-fundamental-when-using-amdf-for-pitch-extraction

    frequency_Hz = 0;
    period_in_samples = 0;
    d = amdf(cur_frame);
   
    d_flipped = -d; % valleys are peaks now
    %[peaks, locs] = findpeaks(d_flipped, 'minpeakprominence', 0.02);
    [peaks, locs] = findpeaks(d_flipped(1:length(d)/4 ));
    %peaks_matrix = [peaks; locs];

    %[first_peak_y, first_peak_loc_index] = max(peaks_matrix(1,:));
    %first_peak_loc = peaks_matrix(2, first_peak_loc_index);
    
    if length(locs) >= 1
        
         for j=1:length(peaks)
            if peaks(j) >= -0.009
                period_in_samples = locs(j); % take first peak over threshold
                break
            end
        end
        
        frequency_Hz = fs / period_in_samples;
    else
        frequency_Hz = 0;
        period_in_samples = 0;
    end
    
end