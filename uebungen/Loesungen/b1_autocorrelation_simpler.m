%% signals
%cd '.\audio'
[w, fs] = audioread('clarinet_A4_fortissimo.mp3');
%[w, fs] = audioread('A4_clarinet.wav');
%[w, fs] = audioread('saxophone_A4.mp3');
%[w, fs] = audioread('my_clarinet_star_wars_main.wav');
%[w, fs] = audioread('Stephen_Fry_cutted.wav');

%[w, fs] = audioread('my_clarinet_imperial_march.wav');
%[w, fs] = audioread('my_clarinet_007.wav');

%[w, fs] = audioread('saxophone_A4.mp3');
%[w, fs] = audioread('contrabassoon_Gis3.mp3');
%[w, fs] = audioread('french-horn_A2.mp3');
%[w, fs] = audioread('cello_D5.mp3');
%[w, fs] = audioread('clarinet_star_wars_main.wav');

%[w, fs] = audioread('program_in_c_cutted.wav');
%[w, fs] = audioread('clarinet_cantina_cutted.wav');
%[w, fs] = audioread('imperial_march_cutted.wav');

%[w, fs] = audioread('flute_Cis7_.mp3');

w = w(:,1); % only get first channel
%w =  w  + 0.05 * randn(size(w));
p = audioplayer(w, fs);
p.play();
frame_length = round(fs*0.03); % length of the analysis frame, 30 ms

%% padd w's and frame's lenght to next power of 2 
zero_samples = 2^nextpow2(length(w)) - length(w);
padding = zeros(1, zero_samples);
w = [transpose(w) padding]; % % transpose w to get row vector to append padding to w
length_w = length(w);
frame_length = 2^nextpow2(frame_length);
%%
lastFrame = length_w/frame_length;

frequency = [];
notes = [];
cents = [];
%% Start
for k = 1 : lastFrame
    range = (k-1)*frame_length + 1:k*frame_length;
    cur_frame = w(range); 
%     cur_frame = [0.5*cur_frame_(1:end/4), 8*cur_frame_(end/4 + 1:end/2)];
%     cur_frame = [cur_frame, 2*cur_frame_(end/2 + 1 : 3*end/4)];
%     cur_frame = [cur_frame, cur_frame_(3*end/4 + 1 : end)];
    %window = transpose(gausswin(frame_length));
    %cur_frame = cur_frame .* window;
       
    %% Autocorrelation to detect local pitch of the frame
    [period_in_samples, frequency_Hz] = calcFrequencyAutoCorr(cur_frame, fs, 1);
    
    %% AMDF
    %[period_in_samples, frequency_Hz] = calcFrequencyAMDF(cur_frame, fs);
    
    %% NMDF (YIN step 2 - 4)
    %[period_in_samples, frequency_Hz, d] = calcFrequencyNMDF(cur_frame, fs);
    %% FFT
    %frequency_Hz = calcFrequencyFFT(cur_frame, fs);
    
    %% Calc frequency to note
    frequency = [frequency frequency_Hz];
    [note, cent] = freqToNote(frequency_Hz);
    notes = [notes, note];
    cents = [cents, cent];
    
end

%% Print Frequency and notes
frequency
notes_cents_matrix = [notes; cents];
disp(notes_cents_matrix);

