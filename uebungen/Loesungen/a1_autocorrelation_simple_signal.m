
% signal
fs = 44100;
T = 1/fs;
N = 3000; % desired length of signal
t = (0:N-1)*T; %time vector 
x = sin(2*pi*440*t); % A4
%x = sin(2*pi*27.5*t); % A0
%x = sin(2*pi*29.135*t); % A#0
%x = sin(2*pi*103.83*t); % G#2
%x = sin(2*pi*261.63*t); % C4
%x = sin(2*pi*1975.5*t); % H6, note error
%x = sin(2*pi*4186*t); % C8, octave error (autocorr)

% %signal 2
% fs = 500;
% T = 1/fs;
% N = 250; % desired length of signal
% t = (0:N-1)*T; %time vector 
% f1 = 8; f2=f1*2; 
% x = sin(2*pi*f1*t-pi/2) + sin(2*pi*f2*t);


% x = [sin(2*pi*29.135*t(1:end/4)), 4*sin(2*pi*29.135*t(end/4 + 1:end))]; % A#0
% x = [x, 3*sin(2*pi*29.135*t(end/2 + 1 : 3*end/4))];
% x = [x, sin(2*pi*29.135*t(3*end/4 + 1 : end))];

figure
subplot(2,1,1);
plot(x);
title('Signal');
xlabel('Sample');
ylabel('Amplitude');
zoom xon;

%% Autocorrelation
[period_in_samples, frequency_Hz, cormx, lags] = calcFrequencyAutoCorr(x, fs, 1);

subplot(2,1,2);
plot(lags, cormx);

title(['Autocorrolated Signal. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
xlabel('Lag');
ylabel('Autocorrelation Measure');
zoom xon;

%% Average Magnitude Function

% lags_amdf = (0:length(x)- 1);
% [period_in_samples, frequency_Hz, d] = calcFrequencyAMDF(x, fs);
% 
% subplot(2,1,2);
% plot(lags_amdf, d);
% title(['AMDF. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
% xlabel('Lag');
% ylabel('AMDF Measure');

%% YIN step 2 - 4

% lags_yin = (0:length(x)- 1);
% [period_in_samples, frequency_Hz, d] = calcFrequencyNMDF(x, fs);
% 
% 
% subplot(2,1,2);
% plot(lags_yin, d);
% title(['YIN. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
% xlabel('Lag');
% ylabel('Amplitude');

%% FFT

% bins = (0: length(x) - 1);
% frequency_Hz_all = bins*fs/length(x); % To convert from bins to Hz
% length_half = length(x) / 2;
% 
% [frequency_Hz, mag] = calcFrequencyFFT(x, fs);
% 
% subplot(2,1,2);
% plot(frequency_Hz_all, mag);
% plot(frequency_Hz_all(1:length_half), mag(1:length_half));
% title(['FFT. Estimated Frequency (' num2str(frequency_Hz) 'Hz)']);
% xlabel('Frequency (Hz)');
% ylabel('Amplitude');
% zoom xon;
  
%% Print frequency and note
[note, cent] = freqToNote(frequency_Hz);
fprintf('%dHz %s %d cent',frequency_Hz, note, cent); 
