function [AMDF] = amdf(signal)
    
    L = length(signal);
   
    AMDF = ones(1, L);
    
    for j = 0:L-1
        AMDF(j+1) = sum(abs(signal(1:L-j) - signal(j+1:end)))/L;
    end
end