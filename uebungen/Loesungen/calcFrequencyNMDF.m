function [period_in_samples, frequency_Hz, d_nmdf] = calcFrequencyNMDF(cur_frame, fs)

    frequency_Hz = 0;
    period_in_samples = 0;
  
    d = asdf(cur_frame);
   
    d_nmdf(1) = 1;
    
    for j = 1:length(cur_frame)-1
        d_nmdf(j+1) = d(j) / ( sum(d(1:j)) / j);
    end
    
    d_flipped = -d_nmdf; % valleys are peaks now
    
    %subplot(3,1,3)
    %plot(d_flipped);
    
    [peaks, locs] = findpeaks(d_flipped);
    
    if length(locs) >= 1 
        
        for j=1:length(peaks)
            if peaks(j) >= -0.3
                period_in_samples = locs(j); % take first peak over threshold
                break
            end
        end
        
        %period_in_samples = locs(1);
        %period_in_samples = first_peak_loc;
    else % take gloabl minimum
        [peaks, locs] = findpeaks(d_flipped);
            peaks_matrix = [peaks; locs];
        if length(locs) >= 1 
            [first_peak_y, first_peak_loc_index] = min(peaks_matrix(1,:));
            first_peak_loc = peaks_matrix(2, first_peak_loc_index);
            period_in_samples = first_peak_loc;
        end
    end
    frequency_Hz = fs / period_in_samples;
end