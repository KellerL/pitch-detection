% signals
%cd '.\audio'
[w, fs] = audioread('clarinet_A4_fortissimo.mp3');
%[w, fs] = audioread('A4_clarinet.wav');
%[w, fs] = audioread('saxophone_A4.mp3');
%[w, fs] = audioread('my_clarinet_star_wars_main.wav');
%[w, fs] = audioread('Stephen_Fry_cutted.wav');

%[w, fs] = audioread('my_clarinet_imperial_march.wav');
%[w, fs] = audioread('my_clarinet_007.wav');

%[w, fs] = audioread('saxophone_A4.mp3');
%[w, fs] = audioread('contrabassoon_Gis3.mp3');
%[w, fs] = audioread('french-horn_A2.mp3');
%[w, fs] = audioread('cello_D5.mp3');
%[w, fs] = audioread('clarinet_star_wars_main.wav');

w = w(:,1); % only get first channel
%w =  w  + 0.05 * randn(size(w));
p = audioplayer(w, fs);
p.play();
frame_length = round(fs*0.03); % length of the analysis frame, 30 ms

%% padd w's and frame's lenght to next power of 2 
zero_samples = 2^nextpow2(length(w)) - length(w);
padding = zeros(1, zero_samples);
w = [transpose(w) padding]; % % transpose w to get row vector to append padding to w
length_w = length(w);
frame_length = 2^nextpow2(frame_length);
%%
lastFrame = length_w/frame_length;

frequency = [];
notes = [];
cents = [];

%w = transpose(w); % to get column vector

%% Start
for k = 1 : lastFrame
    range = (k-1)*frame_length + 1:k*frame_length;
    cur_frame_ = w(range); 
    cur_frame = [0.5*cur_frame_(1:end/4), 8*cur_frame_(end/4 + 1:end/2)];
    cur_frame = [cur_frame, 2*cur_frame_(end/2 + 1 : 3*end/4)];
    cur_frame = [cur_frame, cur_frame_(3*end/4 + 1 : end)];
    %window = transpose(gausswin(frame_length));
    %cur_frame = cur_frame .* window;
    
    subplot(3,1,1);
    plot(w, 'b');
    hold on
    cur_signal = ones(size(w))*NaN; % set size of cur_siganl to the size of w
    cur_signal(range) = cur_frame; % get range (frame) from cur_siganl to plot this red
    plot(cur_signal,'r');
    title('Audio Signal');
    xlabel('Samples');
	ylabel('Amplitude');
    zoom xon;
    hold off
    
    subplot(3,1,2)
    plot(cur_frame);
    title('Current Frame');
    xlabel('Samples');
    ylabel('Amplitude');
    zoom xon;
    
    
    %% Autocorrelation to detect local pitch of the frame
    [period_in_samples, frequency_Hz, cormx, lags] = calcFrequencyAutoCorr(cur_frame, fs, 1);
    
    subplot(3,1,3)
    plot(lags, cormx, 'r')
    title(['Autocorrolated Signal of Current Frame. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
    xlabel('Lag');
    ylabel('Correlation Measure');
    zoom xon;
    
    
    %% AMDF
%     lags_amdf = (0:frame_length - 1);
%     [period_in_samples, frequency_Hz, d] = calcFrequencyAMDF(cur_frame, fs);
%     
%     subplot(3,1,3)
%     plot(lags_amdf, d);
%     title(['AMDF. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
%     xlabel('Lag');
%     ylabel('AMDF Measure');
%     zoom xon;
    

    %% YIN step 2 - 4
    
%     lags_mndf = (0:frame_length - 1);
%     [period_in_samples, frequency_Hz, d] = calcFrequencyNMDF(cur_frame, fs);
%     
%     subplot(3,1,3)
%     plot(lags_mndf, d);
%     title(['MNDF. Period estimated = ' num2str(period_in_samples) ' samples (' num2str(frequency_Hz) 'Hz)']);
%     xlabel('Lag');
%     ylabel('MNDF Measure');
%     zoom xon;


      %% FFT
%     bins = (0: frame_length - 1);
%     frequency_Hz_all = bins*fs/frame_length; % To convert from bins to Hz
%     length_half = frame_length / 2;
%     [frequency_Hz, mag] = calcFrequencyFFT(cur_frame, fs);
% 
%     subplot(3,1,3);
%     %plot(frequency_Hz_all, mag);
%     plot(frequency_Hz_all(1:length_half), mag(1:length_half));
%     title(['FFT. Estimated Frequency ' num2str(frequency_Hz) 'Hz']);
%     xlabel('Frequency (Hz)');
%     ylabel('Magnitude');
%     zoom xon;
%     
%     
    %% Calc frequency to note
    [note, cent] = freqToNote(frequency_Hz);
    fprintf('%d Frequency \n',frequency_Hz); 
    fprintf('%s %d cent \n', note, cent); 

    
    frequency = [frequency frequency_Hz];
    [note, cent] = freqToNote(frequency_Hz);
    notes = [notes, note];
    cents = [cents, cent];
    
    
    pause

end
%% Print Frequency and notes
frequency
notes_cents_matrix = [notes; cents];
disp(notes_cents_matrix);


