function [ASDF] = asdf(signal)
    
    L = length(signal);
   
    ASDF = ones(1, L);
    
    for j = 0:L-1
        ASDF(j+1) = sum((signal(1:L-j) - signal(j+1:end)).^2);
    end
end