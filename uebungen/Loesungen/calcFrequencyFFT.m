function [frequency_Hz, mag] = calcFrequencyFFT(x, fs)

    mag = abs(fft(x));
    bins = (0:length(x)-1);
    frequency_Hz_all = bins*fs/length(x); % To convert from bins to Hz
    frequency_Hz = 0;
    
    % just picking highest magniutde is not sufficient => Improve by Harmonic product spectrum
    % or harmonic sum product

    % pick lowest frequency (first position of locs)
    [peaks, locs] = findpeaks(mag, frequency_Hz_all);
    if length(locs) >= 1
        for j=1:length(peaks)
            if peaks(j) >= max(peaks)/2
                frequency_Hz = locs(j); % take first peak over threshold
                break
            end
        end
    else frequency_Hz = 0;
    end
    


end