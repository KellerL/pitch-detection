% calculates the frequency by 
function [period_in_samples, frequency_Hz, cormx, lags] = calcFrequencyAutoCorr(cur_frame, fs, window)
   
    % Period of signal is equal to the distance between the two highest peaks
    % in the autocorrelation signal
    % Identify most prominent peaks
    % Most prominent peak will be at the center of the correlation function

    
    if window == 1
        [cormx, lags] = xcorr(cur_frame, cur_frame);
    else
        [cormx1, lags] = xcorr(cur_frame, cur_frame);
        [cormx2, ] = xcorr(window, window);
        cormx = cormx1 ./ cormx2;
       % lags = (-length(cur_frame):length(cur_frame));
    end
    
    % get first peak
    [peaks, locs] = findpeaks(cormx, lags);
    peaks_matrix = [peaks; locs];

    [first_peak_y, first_peak_loc_index] = max(peaks_matrix(1,:));
    first_peak_loc = peaks_matrix(2, first_peak_loc_index);

    % delete first_peak from peaks_matrix 
    peaks(first_peak_loc_index) = [];
    locs(first_peak_loc_index) = [];
    peaks_matrix = [peaks; locs];

    % get second peak
    [second_peak_y, second_peak_loc_index] = max(peaks_matrix(1,:));
    second_peak_loc = peaks_matrix(2, second_peak_loc_index);

    period_in_samples =  abs(second_peak_loc - first_peak_loc);

    if(period_in_samples ~= 0)
        frequency_Hz = fs / period_in_samples;
        % Adjust frequency_Hz by subtracting error ratio of autocorrelation
        %frequency_Hz = frequency_Hz - (1 + 0.5 * (frequency_Hz / fs));
    else
        frequency_Hz = 0;
    end
    
end