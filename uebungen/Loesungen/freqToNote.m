function [note, cent] = freqToNote(estimated_freq)

 % https://newt.phys.unsw.edu.au/music/note/ JavaScript version
 
    if estimated_freq < 27.5 | estimated_freq > 14080
        note = NaN;
        cent = NaN;
    else
        A4 = 440.0;
        A4_index = 58;
        notes = [
                    "C0","C#0","D0","D#0","E0","F0","F#0","G0","G#0","A0","A#0","B0" ...
                    "C1","C#1","D1","D#1","E1","F1","F#1","G1","G#1","A1","A#1","B1" ...
                    "C2","C#2","D2","D#2","E2","F2","F#2","G2","G#2","A2","A#2","B2" ...
                    "C3","C#3","D3","D#3","E3","F3","F#3","G3","G#3","A3","A#3","B3" ...
                    "C4","C#4","D4","D#4","E4","F4","F#4","G4","G#4","A4","A#4","B4" ...
                    "C5","C#5","D5","D#5","E5","F5","F#5","G5","G#5","A5","A#5","B5" ...
                    "C6","C#6","D6","D#6","E6","F6","F#6","G6","G#6","A6","A#6","B6" ...
                    "C7","C#7","D7","D#7","E7","F7","F#7","G7","G#7","A7","A#7","B7" ...
                    "C8","C#8","D8","D#8","E8","F8","F#8","G8","G#8","A8","A#8","B8" ...
                    "C9","C#9","D9","D#9","E9","F9","F#9","G9","G#9","A9","A#9","B9" ];

        minus = 0;
        plus = 1;
        freq = A4;
        r = 2^(1.0/12.0);
        cent = 2^(1.0/1200.0);
        r_index = 0;
        cent_index = 0;
        side = plus;

        if estimated_freq >= freq
            while estimated_freq >= r * freq
                  freq = r * freq;
                  r_index = r_index + 1;
            end
            while estimated_freq > cent * freq
                  freq = cent * freq;
                  cent_index = cent_index + 1;
            end
            if (cent * freq - estimated_freq) < (estimated_freq - freq)
                cent_index = cent_index + 1;
            end
            if cent_index > 50
                r_index = r_index + 1;
                cent_index = 100 - cent_index;
                if cent_index ~= 0
                    side = minus;
                end
            end


        else
            while estimated_freq <= freq / r
                freq = freq / r;
                r_index = r_index - 1;
            end
            while estimated_freq < freq / cent
                freq = freq / cent;
                cent_index = cent_index + 1;
            end
            if (estimated_freq - freq / cent) < (freq - estimated_freq)
                cent_index = cent_index + 1;
            end
            if cent_index >= 50
                r_index = r_index - 1;
                cent_index = 100 - cent_index;
            else
                if cent_index ~= 0
                    side = minus;
                end
            end

        end

        note = notes(A4_index + r_index);
        cent = cent_index;
        if side == minus
            cent = -1 * cent_index;
        end
    end
end