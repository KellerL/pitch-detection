
%% https://dadorran.wordpress.com/category/matlab-code/page/1/
% Single-sided magnitude spectrum with frequency axis in Hertz
% 
% Each bin frequency is separated by fs/N Hertz.
% 
% X_mags = abs(fft(signal));
% bin_vals = [0 : N-1];
% fax_Hz = bin_vals*fs/N;
% N_2 = ceil(N/2);
% plot(fax_Hz(1:N_2), X_mags(1:N_2))
% xlabel('Frequency (Hz)')
% ylabel('Magnitude');
% title('Single-sided Magnitude spectrum (Hertz)');
% axis tight

%% Time Domain
Fs = 44100;           % Sampling frequency, 44.1 kHz              
T = 1/Fs;             % Sampling period       
L = 1500;             % Length of signal, 1.5 seconds
t = (0:L-1)*T;        % Time vector

a = sin(2*pi*440*t);  % A4 440 Hz

%f1 = 8; f2=f1*2; 
%x = sin(2*pi*f1*t-pi/2) + sin(2*pi*f2*t);

figure
subplot(2,1,1);
plot(t, a);
title('Time Domain of note A4');
xlabel('t (seconds)');
ylabel('Amplitude');
zoom xon;

%% Frequency Domain
A_mag = abs(fft(a));
bins = (0:L-1);
A_Hz = bins*Fs/L; % To convert from bins to Hz
L_2 = ceil(L/2); 
subplot(2,1,2);

plot(A_Hz(1:L_2), A_mag(1:L_2)); % 1 in (1:L_2) due to MatLab index starting at 1
title('Single-sided Magnitude spectrum of Note A');
xlabel('f (Hz)');
ylabel('Magnitude');

%% Inverse FFT

Y = ifft(A_mag);



