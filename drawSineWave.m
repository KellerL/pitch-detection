%% References
% https://www.mathworks.com/matlabcentral/answers/36428-sine-wave-plot   
% https://www.mathworks.com/help/matlab/ref/fft.html

%% Time specifications:
   Fs = 8000;            % samples per second, sampling frequency
   dt = 1/Fs;            % seconds per sample, sampling period
   L = 0.25;             % seconds, length of signal
   t = (0:dt:L-dt)';     % seconds, time vector

   %% Sine wave:
   Fc = 60;                     % hertz
   x = cos(2*pi*Fc*t);

   % Plot the signal versus time:
   figure;
   plot(t,x);
   xlabel('time (in seconds)');
   title('Signal versus Time');
   zoom xon;
   
   %% Convert signal to frequency domain, added by me
   
   y = fft(x);
   % Compute the two-sided spectrum P2. Then compute the single-sided spectrum P1 based on P2 and the even-valued signal length L.
   P2 = abs(y/L);
   P1 = P2(1:L/2+1);
   P1(2:end-1) = 2*P1(2:end-1);
   % Define frequency domain f and plot the single-sided amplitude spectrum P1
   f = Fs*(0:(L/2))/L;
   plot(f,P1) 
   title('Single-Sided Amplitude Spectrum of X(t)')
   xlabel('f (Hz)')
   ylabel('|P1(f)|')
   
   zoom xon;
   